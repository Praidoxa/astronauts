package com.example.app.services;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.app.domain.Astronaut;

@Service
public class Initialization {

	private long leftLimit = 60L;
	long rightLimit = 90L;

	public List<Astronaut> initialize() {
		return Arrays.asList(new Astronaut(1, "Anton Shkaplerov", generate(), "ISS"),
				new Astronaut(2, "Scott Tingle", generate(), "ISS"),
				new Astronaut(3, "Norishige Kanai", generate(), "ISS"),
				new Astronaut(4, "Oleg Artemyev", generate(), "ISS"),
				new Astronaut(5, "Andrew Feustel", generate(), "ISS"),
				new Astronaut(6, "Richard Arnold", generate(), "ISS"));
	}

	private Long generate() {
		return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
	}
}
