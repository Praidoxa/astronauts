package com.example.app.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.app.domain.Astronaut;
import com.example.app.repositories.IRepository;
import com.example.app.services.Initialization;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class Controller {
	
	@Autowired
	IRepository iRepository;
	
	@Autowired
	Initialization initialization;
	
	//Get Astronauts
	@GetMapping("/getAll")
	public List<Astronaut> findAll() throws Exception{
		 iRepository.saveAll(initialization.initialize());
		return (List<Astronaut>) Optional.ofNullable(iRepository.findAll()).orElseThrow(NotFoundException::new);
	}
	
	//Get Astronaut by id 
	//for test
	@GetMapping("/{id}")
	public Astronaut getOne(@PathVariable int id) {
		return iRepository.findById(id).orElse(null);
	}

}
